#include <SPI.h>
#include <MFRC522.h>
#include <Keypad.h>

#define RST_PIN         9          // Configurable, see typical pin layout above
#define SS_PIN          10         // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance

const byte rows = 4;
const byte cols = 4;

char keyMap [rows] [cols] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'},
};

byte rowPins [rows] = {A3, 2, 3, 4};
byte colPins [cols] = {5, 6, 7, 8};

Keypad myKeypad = Keypad( makeKeymap(keyMap), rowPins, colPins, rows, cols );

void setup() {
    Serial.begin(9600);     // Initialize serial communications with the PC
    while (!Serial);        // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();            // Init SPI bus
    mfrc522.PCD_Init();     // Init MFRC522

    //mfrc522.PCD_DumpVersionToSerial();    // Show details of PCD - MFRC522 Card Reader details
    //Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
}

void loop() {
      hendleKeypad();
    // Look for new cards
  String code = "";
    if ( ! mfrc522.PICC_IsNewCardPresent()) {
        return;
    }
    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial()) {
        return;
    }
  
  for (byte i = 0; i < mfrc522.uid.size; i++) {
      code += String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" :" ");
      code += String(mfrc522.uid.uidByte[i], HEX);
  }
  code.toUpperCase();
  Serial.println(code);
  Serial.write("code");
  mfrc522.PICC_HaltA();
  
 
}
void hendleKeypad() {
  char pressedKey = myKeypad.getKey();

  if (pressedKey == NO_KEY) {
    return;
  }
  //Serial.write("Pressed key: ");
  Serial.write(pressedKey);
  Serial.write("\n");
  
  delay(100);
}
